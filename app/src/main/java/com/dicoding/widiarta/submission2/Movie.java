package com.dicoding.widiarta.submission2;
import android.os.Parcel;
import android.os.Parcelable;

public class Movie implements Parcelable {
    private int photo;
    private String name;
    private String description;
    private String tahunRilis;
    private String durasi;
    private String rating;
    private String genre;
    private int aktor1;
    private int aktor2;
    private int aktor3;

    public int getAktor1() {
        return aktor1;
    }

    public void setAktor1(int aktor1) {
        this.aktor1 = aktor1;
    }

    public int getAktor2() {
        return aktor2;
    }

    public void setAktor2(int aktor2) {
        this.aktor2 = aktor2;
    }

    public int getAktor3() {
        return aktor3;
    }

    public void setAktor3(int aktor3) {
        this.aktor3 = aktor3;
    }

    public String getTahunRilis() {
        return tahunRilis;
    }

    public void setTahunRilis(String tahunRilis) {
        this.tahunRilis = tahunRilis;
    }

    public String getDurasi() {
        return durasi;
    }

    public void setDurasi(String durasi) {
        this.durasi = durasi;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.photo);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.tahunRilis);
        dest.writeString(this.durasi);
        dest.writeString(this.rating);
        dest.writeString(this.genre);
        dest.writeInt(this.aktor1);
        dest.writeInt(this.aktor2);
        dest.writeInt(this.aktor3);
    }

    public Movie() {
    }

    protected Movie(Parcel in) {
        this.photo = in.readInt();
        this.name = in.readString();
        this.description = in.readString();
        this.tahunRilis = in.readString();
        this.durasi = in.readString();
        this.rating = in.readString();
        this.genre = in.readString();
        this.aktor1 = in.readInt();
        this.aktor2 = in.readInt();
        this.aktor3 = in.readInt();
    }

    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}