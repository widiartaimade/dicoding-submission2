package com.dicoding.widiarta.submission2;


import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class TvSeriesFragment extends Fragment {

    private String title = "Mode TV Series";
    private RecyclerView rvMovie;
    private ArrayList<Movie> list = new java.util.ArrayList<>();
    private final String STATE_TITLE = "state_string";
    private final String STATE_LIST = "state_list";
    private final String STATE_MODE = "state_mode";
    private String[] dataName, dataDescription, genre, rating, durasi, tahunRilis;
    private TypedArray dataPhoto, dataImgAktor1, dataImgAktor2, dataImgAktor3;


    public TvSeriesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tv_series, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        rvMovie = (RecyclerView) getView().findViewById(R.id.rv_series);
        rvMovie.setHasFixedSize(true);

        if (savedInstanceState == null) {

            prepare();
            list.addAll(getListData());
            showRecyclerList();

        } else {

            //title = savedInstanceState.getString(STATE_TITLE);
            ArrayList<Movie> stateList = savedInstanceState.getParcelableArrayList(STATE_LIST);
            int stateMode = savedInstanceState.getInt(STATE_MODE);


            if (stateList != null) {
                list.addAll(stateList);
            }

        }
    }

    private void showRecyclerList() {
        rvMovie.setLayoutManager(new LinearLayoutManager(getContext()));
        ListMovieAdapter listHeroAdapter = new ListMovieAdapter(list);
        rvMovie.setAdapter(listHeroAdapter);

        listHeroAdapter.setOnItemClickCallback(new ListMovieAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(Movie data) {
                showSelectedHero(data);
            }
        });
    }

    public ArrayList<Movie> getListData(){
        ArrayList<Movie> list = new ArrayList<>();
        for (int i = 0; i<dataName.length;i++) {
            Movie movie = new Movie();
            movie.setName(dataName[i]);
            movie.setDescription(dataDescription[i]);
            movie.setTahunRilis(tahunRilis[i]);
            movie.setPhoto(dataPhoto.getResourceId(i,-1));
            movie.setRating(rating[i]);
            movie.setDurasi(durasi[i]);
            movie.setGenre(genre[i]);
            movie.setPhoto(dataPhoto.getResourceId(i,-1));
            movie.setAktor1(dataImgAktor1.getResourceId(i,-1));
            movie.setAktor2(dataImgAktor2.getResourceId(i, -1));
            movie.setAktor3(dataImgAktor3.getResourceId(i, -1));
            list.add(movie);
        }
        return list;
    }

    private void prepare(){
        dataName = getResources().getStringArray(R.array.series_movie);
        dataDescription = getResources().getStringArray(R.array.series_description);
        tahunRilis = getResources().getStringArray(R.array.series_rilis);
        rating = getResources().getStringArray(R.array.rating);
        durasi = getResources().getStringArray(R.array.series_durasi);
        genre = getResources().getStringArray(R.array.genre);
        dataPhoto = getResources().obtainTypedArray(R.array.series_poster);
        dataImgAktor1 = getResources().obtainTypedArray(R.array.img_aktor1);
        dataImgAktor2 = getResources().obtainTypedArray(R.array.img_aktor2);
        dataImgAktor3 = getResources().obtainTypedArray(R.array.img_aktor3);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_TITLE, title);
        outState.putParcelableArrayList(STATE_LIST, list);
        //outState.putInt(STATE_MODE, mode);
    }

    private void showSelectedHero(Movie movie) {
        //Toast.makeText( getActivity() , movie.getName(), Toast.LENGTH_LONG).show();
        Intent moveWithObjectIntent2 = new Intent(getActivity(), DetailMovie.class);
        moveWithObjectIntent2.putExtra(DetailMovie.EXTRA_TITIPAN, movie);
        startActivity(moveWithObjectIntent2);
    }
}
