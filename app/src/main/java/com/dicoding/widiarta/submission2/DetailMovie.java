package com.dicoding.widiarta.submission2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailMovie extends AppCompatActivity {
    public static final String EXTRA_TITIPAN = "titipan";

    private String[] dataName, durasi, tahunRilis;
    private String[] dataDescription, genre, rating;
    private TypedArray dataPhoto, dataImgAktor1, dataImgAktor2, dataImgAktor3;

    TextView tvJudulFilm, tvTahunRilis, tvSinopsis, tvRating, tvDurasi, tvGenre;
    ImageView imgPoster, imgAktor1, imgAktor2, imgAktor3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movie);

        tvJudulFilm = findViewById(R.id.tvJudulfilm);
        tvTahunRilis = findViewById(R.id.tvThnRilis);
        tvSinopsis = findViewById(R.id.tvSinopsis);
        tvRating = findViewById(R.id.tvRating);
        tvDurasi = findViewById(R.id.tvDurasi);
        tvGenre = findViewById(R.id.tvGenreFilm);
        imgPoster = findViewById(R.id.img_poster);
        imgAktor1 = findViewById(R.id.img_Aktor1);
        imgAktor2 = findViewById(R.id.img_Aktor2);
        imgAktor3 = findViewById(R.id.img_Aktor3);

        prepare();

        Movie titipan = getIntent().getParcelableExtra(EXTRA_TITIPAN);
        tvJudulFilm.setText(titipan.getName());
        tvTahunRilis.setText(titipan.getTahunRilis());
        tvRating.setText(titipan.getRating());
        tvDurasi.setText(titipan.getDurasi());
        tvGenre.setText(titipan.getGenre());
        tvSinopsis.setText(titipan.getDescription());
        imgPoster.setImageResource(titipan.getPhoto());
        imgAktor1.setImageResource(titipan.getAktor1());
        imgAktor2.setImageResource(titipan.getAktor2());
        imgAktor3.setImageResource(titipan.getAktor3());
    }

    private void prepare(){
        dataName = getResources().getStringArray(R.array.data_name);
        dataDescription = getResources().getStringArray(R.array.data_description);
        tahunRilis = getResources().getStringArray(R.array.tahun_rilis);
        rating = getResources().getStringArray(R.array.rating);
        durasi = getResources().getStringArray(R.array.durasi);
        genre = getResources().getStringArray(R.array.genre);
        dataPhoto = getResources().obtainTypedArray(R.array.data_photo);
        dataImgAktor1 = getResources().obtainTypedArray(R.array.img_aktor1);
        dataImgAktor2 = getResources().obtainTypedArray(R.array.img_aktor2);
        dataImgAktor3 = getResources().obtainTypedArray(R.array.img_aktor3);
    }
}

